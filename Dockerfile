FROM mithoopolly/rnaseq-downstream:prod_v0.2.0
USER root

ARG DEBIAN_FRONTEND=noninteractive
RUN echo ""| sudo tee /srv/main.sh
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

COPY local_mapping_file.csv /home/local_mapping_file.csv
COPY rnorvegicus_metadata.tsv.cut /home/rnorvegicus_metadata.tsv.cut
COPY make_gct.R /home/make_gct.R
COPY push_gct.py /home/push_gct.py
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
