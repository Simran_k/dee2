import pandas as pd
import quilt3

GEO_data_lake = "GEO_data_lake/alternative"
GEO_repo_id = 9
bucket_name = "s3://discover-prod-datalake-v1"

# GEO_data_lake = quilt3.Package.browse(GEO_data_lake, bucket_name)

import quilt3
import json
import os
import logging

logger = logging.getLogger(__name__)

try:
    from discoverpy import Discover, download_helper, constants as cnst, env
except Exception:
    logger.warn("Unable to import discoverpy. Dataset will not be pushed to data lake")


def publish_dataset(quilt_path, filepath, repo_id,
                    curation_json_filepath=None):
    """Function for pushing a dataset to a data repository on Polly.

    Parameters
    ----------
    filepath : str
        path of the file
    repo_id : int
        repository id on Polly
    dataset_type: str
        type of dataset. one of ["Single cell", "Metabolomics", "Proteomics", "Transcriptomics"]
    dataset_params : dict
        a dictionary containing the various dataset params such as dataset_id, organism, source, description
    curation_json_filepath : str, optional
        curation json filepath generated after running tagging using the curation API
    """
    curr_exec_env = env.get_curr_exec_env()
    print(f"Current execution environment: {curr_exec_env}")
    print(f"Current execution environment: {curr_exec_env}")
    if curr_exec_env == "test":
        quilt_s3_bucket = "s3://discover-test-datalake-v1"
    else:
        quilt_s3_bucket = "s3://discover-prod-datalake-v1"

    logger.info("Checking for access to repository")
    repositories = Discover().get_repositories()
    access_repositories = [k_["repo_id"] for k_ in repositories]
    if repo_id in access_repositories:
        logger.info("Access verified")
    else:
        logger.error(
            f"You do not have access to repository with id: {repo_id}"
        )

    logger.info("Setting credentials for pushing to repository")
    set_aws_credentials(repo_id)
    
    logger.info("Preparing json for filter screen")
    # quilt_meta = get_quilt_meta(curation_json_filepath, dataset_params)
    # quilt_meta["kw_data_type"] = dataset_type
    # available_packages = quilt3.Package.list("s3://discover-prod-datalake-v1")

    repo_package = get_repo_package(repo_id)
    logger.info(f"Repo package: {repo_package}")
    if repo_package == "":
        logger.error(
            "Could not find corresponding package for this repository."
        )
        raise Exception("Could not find corresponding package for this repository")
    logger.info(f"s3 bucket: {quilt_s3_bucket}")
    try:
        # TO DO: get package name from repo_id
        quilt_pkg = quilt3.Package.browse(repo_package, quilt_s3_bucket)
    except Exception as e:
        logger.info(e)
        logger.error(
            "could not access repository package"
        )

    logger.info("Adding to package")
    # quilt_pkg_data_path = os.path.join(
    #     dataset_params["dataset_id"], os.path.basename(filepath)
    # )
    quilt_pkg_data_path = quilt_path
    quilt_pkg.set(quilt_pkg_data_path, filepath, meta=curation_json_filepath)

    logger.info("Pushing package")
    quilt_pkg.push(repo_package, quilt_s3_bucket)


def set_aws_credentials(repo_id):
    import os

    # Fetch sts tokens
    tokens = download_helper.get_tokens(repo_id)

    # set credentials in aws credentials file path
    os.system("mkdir ~/.aws")
    HOME_DIR = os.getenv("HOME")

    fp = open(HOME_DIR + "/.aws/credentials", "w")
    fp.write("[default]\n")
    fp.write(f'aws_access_key_id = {tokens["AccessKeyId"]}\n')
    fp.write(f'aws_secret_access_key = {tokens["SecretAccessKey"]}\n')
    if "SessionToken" in tokens:
        fp.write(f'aws_session_token = {tokens["SessionToken"]}')
    fp.close()


def get_quilt_meta(curation_json_filepath, dataset_params):

    quilt_meta = dict()
    quilt_meta["__index__"] = {"data_required": "false"}
    quilt_meta["file_type"] = "gct"

    if os.path.exists(curation_json_filepath):
        print("Reading tissue and disease from curation json")
        with open(curation_json_filepath) as f:
            curation_dict = json.load(f)

        quilt_meta["tissue"] = list(
            set(
                [
                    c["kw_extracted_from"]
                    for c in curation_dict["concepts"]
                    if c["kw_concept_type"] == "tissue/cell_line"
                ]
            )
        )
        quilt_meta["disease"] = list(
            set(
                [
                    c["kw_extracted_from"]
                    for c in curation_dict["concepts"]
                    if c["kw_concept_type"] == "disease"
                ]
            )
        )
    else:
        logger.warning("curation json file path not provided. tissue and disease tags will not be added")
        quilt_meta["tissue"] = list()
        quilt_meta["disease"] = list()

    quilt_meta["dataset_id"] = dataset_params["dataset_id"]
    quilt_meta["organism"] = (
        dataset_params["organism"]
        if "organism" in dataset_params
        else "unknown_organism"
    )
    quilt_meta["dataset_source"] = "GEO"
    quilt_meta["platform"] = (
        dataset_params["platform"]
        if "platform" in dataset_params
        else "unknown_platform"
    )
    quilt_meta["description"] = (
        dataset_params["title"] if "title" in dataset_params else "unknown_description"
    )
    return quilt_meta


def get_repo_package(repo_id):
    REPOSITORY_PACKAGE_ENDPOINT = cnst.REPOSITORIES_ENDPOINT + "/{}/packages"
    url = env.DISCOVER_API_URL + str.format(REPOSITORY_PACKAGE_ENDPOINT, repo_id)
    resp = env.make_discover_request("GET", url)
    if resp.status_code == cnst.OK:
        data = resp.json().get("data")
        if len(data) > 1:
            logger.error(
                "Multiple packages exist for this repo id.",
            )
            raise Exception("Multiple packages exist for this repo id")
        if len(data) == 1:
            package_name = data[0].get("attributes").get("package_name")
            return package_name
        return ""
    else:
        print(resp.text)


study_metadata = pd.read_csv("study_metadata.csv")
study_metadata = study_metadata.T
study_metadata.reset_index(drop=True, inplace=True)
new_header = study_metadata.iloc[0] #grab the first row for the header
study_metadata = study_metadata[1:] #take the data less the header row
study_metadata.columns = new_header #set the header row as the df header


import glob
count = 0 
for name in glob.glob('curated_gcts/*'): 
    count = count + 1
    print(name) 
    dataset_id = "_".join((name.split("/")[1]).split("_")[0:2])
    print(dataset_id)
    quilt_path = "RNASeq/" + dataset_id.split("_")[0] + "/GCT/" + name.split("/")[1]
    file_path = name
    metadata = study_metadata[study_metadata["dataset"] == dataset_id]
    json = {'__index__': {'data_required': 'true'},
             'publication_name': list(metadata["pubMedIds"])[0],
             'tissue': 'None',
             'dataset_source': 'GEO',
             'description': list(metadata["title"])[0],
             'organism': list(metadata["organism"])[0],
             'year': list(metadata["last_update_date"])[0],
             'disease': "",
             'operation': {'is_normalized': 'true',
              'batch_corrected_variable': 'none',
              'is_batch_corrected': 'false',
              'normalized_type': 'DESEQ-VST'},
             'platform': 'RNASeq',
             'dataset_id': dataset_id,
             'is_public': 'true',
             'data_repository': 'geo',
             'kw_cell_type': "",
             'kw_data_type': 'Transcriptomics',
               'author' : list(metadata["author"])[0],
            'abstract' : list(metadata["abstract"])[0],
            'publication' : list(metadata["url"])[0],
            'overall_design' : list(metadata["overall_design"])[0],
            'summary' : list(metadata["summary"])[0],
            'type' : list(metadata["type"])[0],
            'curation_version' : 'g1'
               }
    publish_dataset(quilt_path, file_path, GEO_repo_id, curation_json_filepath=json)
    # quilt_pkg.set(quilt_path, file_path, meta = json)

# gcts_names

