#!/bin/bash

echo "================================================="

# Checking the version of Polly CLI
# polly --version
# login into Polly with Polly CLI inside a Polly job
# polly login --auto

# RAW_FILE_NAME, SETTINGS_FILE_PATH, DATABASE_FILE_PATH, etc. are system
# environment variables that will be passed by the user when starting a
# Polly job. See the Polly CLI documentation to know how to pass the
# environment to a job.

# echo "======================= DOWNLOAD FROM SOURCE BUCKET =========================="
# aws configure set aws_access_key_id ${SRC_AWS_ACCESS_KEY_ID}
# aws configure set aws_secret_access_key ${SRC_AWS_SECRET_ACCESS_KEY}
# aws s3 cp "${CSV_FILE}" "local_csv_file.csv"
# aws s3 cp "${MAPPING_FILE}" "local_mapping_file.csv"

echo ''

# make curated_gcts folder 
mkdir curated_gcts

# call r script
Rscript /home/make_gct.R

# aws configure set aws_access_key_id ${DEST_AWS_ACCESS_KEY_ID}
# aws configure set aws_secret_access_key ${DEST_AWS_SECRET_ACCESS_KEY}

# call python script
python3 /home/push_gct.py